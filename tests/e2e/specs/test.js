// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {

    cy.visit("https://scherban.daniel.gitlab.io/vue_cypress_test/")
    // cy.contains('h1', 'Welcome to Your Vue.js App')
    cy.get('#login').invoke('attr', 'value', '100').should('have.value', '100')
    cy.get("#submit").click()
  })
})