const { defineConfig } = require("@vue/cli-service");

module.exports = defineConfig({
  // publicPath: process.env.NODE_ENV === "production" ? `/${process.env.CI_PROJECT_NAME}/` : "/",
  publicPath: process.env.NODE_ENV === "production" ? `/vue_cypress_test/` : "/",
  transpileDependencies: true
});
